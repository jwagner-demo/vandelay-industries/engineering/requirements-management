import xml.etree.ElementTree as ET
import json

# Load File
tree = ET.parse('data/test_results.xml')
root = tree.getroot()


json_results = {}


# Loop Through Test Results
main_test_suite = root.find('testsuite')
req_id = 1
for test in main_test_suite:
    
    # See If It Passed Or Failed
    status = "passed"
    if (len(test)) > 0:
        status = "failed"

    # Add To JSON
    json_results[f'{req_id}'] = status

    # Increment ID
    req_id += 1


# Serializing json
json_object = json.dumps(json_results, indent=4)
 
# Writing to requirements.json
with open("requirements.json", "w") as outfile:
    outfile.write(json_object)